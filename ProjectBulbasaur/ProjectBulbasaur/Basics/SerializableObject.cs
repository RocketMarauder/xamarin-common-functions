﻿using GalaSoft.MvvmLight;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ProjectBulbasaur.Basics
{
    public abstract class SerializableObject : ObservableObject
    {
        /// <summary>
        /// Function to convert object to XML.
        /// </summary>
        /// <returns></returns>
        public string ToXml()
        {
            var serializer = new XmlSerializer(GetType());
            var stringBuilder = new StringBuilder();
            var xmlWriter = XmlWriter.Create(stringBuilder);
            serializer.Serialize(xmlWriter, this);
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Function to take XML and Deserialize it into an object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static T FromXml<T>(string xml) where T : SerializableObject
        {
            var serializer = new XmlSerializer(typeof(T));
            var stringReader = new StringReader(xml);
            var xmlReader = XmlReader.Create(stringReader);
            xmlReader.Read();
            var obj = (T)serializer.Deserialize(xmlReader);
            return obj;
        }
    }
}
