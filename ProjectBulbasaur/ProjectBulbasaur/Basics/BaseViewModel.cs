﻿using GalaSoft.MvvmLight;

namespace ProjectBulbasaur.Basics
{
    public class BaseViewModel<T> : ViewModelBase
    {
        private T _model;
        public T Model
        {
            get => _model;
            set
            {
                OnModelChange(_model, value);
                _model = value;
                RaisePropertyChanged(() => Model);
            }
        }

        protected virtual void OnModelChange(T oldModel, T newModel)
        {

        }
    }
}
