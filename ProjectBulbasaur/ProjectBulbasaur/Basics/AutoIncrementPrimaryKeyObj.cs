﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectBulbasaur.Basics
{
    public class AutoIncrementPrimaryKeyObj : SerializableObject
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
    }
}
