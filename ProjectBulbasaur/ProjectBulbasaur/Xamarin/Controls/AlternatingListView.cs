﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ProjectBulbasaur.Xamarin.Controls
{
    public class AlternatingListView : ListView
    {
        public AlternatingListView()
        {

        }

        public static readonly BindableProperty FirstBackgroundColorProperty = BindableProperty.Create(
            "BackgroundColor", typeof(Color), typeof(AlternatingListView), Color.Transparent);

        public Color FirstBackgroundColor
        {
            get { return (Color)GetValue(FirstBackgroundColorProperty); }
            set { SetValue(FirstBackgroundColorProperty, value); }
        }

        public static readonly BindableProperty AlternateBackgroundColorProperty = BindableProperty.Create(
            "AlternativeBackgroundColor", typeof(Color), typeof(AlternatingListView), Color.Transparent);

        public Color AlternateBackgroundColor
        {
            get { return (Color)GetValue(AlternateBackgroundColorProperty); }
            set { SetValue(AlternateBackgroundColorProperty, value); }
        }

        protected override void SetupContent(Cell content, int index)
        {
            base.SetupContent(content, index);

            var viewCell = content as ViewCell;
            viewCell.View.BackgroundColor = index % 2 == 0 ? FirstBackgroundColor : AlternateBackgroundColor;
        }
    }
}
