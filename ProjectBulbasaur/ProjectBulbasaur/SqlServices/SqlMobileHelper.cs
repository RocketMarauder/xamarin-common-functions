﻿using System;
using System.IO;

namespace ProjectBulbasaur.SqlServices
{
    public static class SqlMobileHelper
    {
        public static string Construct_Android_Path()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return path;
        }

        public static string Construct_iOS_Path()
        {
            var personalFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var path = Path.Combine(personalFolder, "..", "Library");
            return path;
        }

        public static string Construct_Android_Path(string dbName)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var finalPath = Path.Combine(path, dbName);
            return finalPath;
        }

        public static string Construct_iOS_Path(string dbName)
        {
            var personalFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var path = Path.Combine(personalFolder, "..", "Library", dbName);
            return path;
        }
    }
}
