﻿using System.IO;
using SQLite;

namespace ProjectBulbasaur.SqlServices
{
    public class SqliteService : ISqliteService
    {
        private static SQLiteAsyncConnection _connection;
        public SQLiteAsyncConnection GetConnection(string path, string dbName)
        {
            if(_connection != null)
            {
                return _connection;
            }

            else
            {
                if (string.IsNullOrEmpty(path) || string.IsNullOrEmpty(dbName))
                {
                    return null;
                }
                else
                {
                    var sqlPath = Path.Combine(path, dbName);
                    _connection = new SQLiteAsyncConnection(sqlPath);
                }
            }

            return _connection;

        }

        public SQLiteAsyncConnection GetConnection(string path)
        {
            if (_connection != null)
            {
                return _connection;
            }

            else
            {
                if (string.IsNullOrEmpty(path))
                {
                    return null;
                }
                else
                {
                    _connection = new SQLiteAsyncConnection(path);
                }
            }

            return _connection;
        }

        public SQLiteAsyncConnection GetConnection()
        {
            return _connection;
        }
    }
}
