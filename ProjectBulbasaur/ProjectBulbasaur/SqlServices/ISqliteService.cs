﻿using SQLite;

namespace ProjectBulbasaur.SqlServices
{
    public interface ISqliteService
    {
        SQLiteAsyncConnection GetConnection(string path, string dbName);
        SQLiteAsyncConnection GetConnection(string path);
        SQLiteAsyncConnection GetConnection();
    }
}
